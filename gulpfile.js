'use strict';

let gulp = require('gulp'),
    plumberNotifier = require('gulp-plumber-notifier'),
    $ = require('gulp-load-plugins')(),
    browserify = require('browserify'),
    cssnano = require('cssnano'),
    colorguard = require('colorguard'),
    atImport = require("postcss-import"),
    cssnext = require("postcss-cssnext"),
	sugarss = require('sugarss'),
    reporter = require('postcss-browser-reporter'),
    cachebuster = require("postcss-cachebuster"),
    inlineSvg = require("postcss-inline-svg"),
    runSequence = require('run-sequence'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    inlineBase64 = require('postcss-inline-base64'),
    //config = require('./package.json'),
    browserSync = require('browser-sync'),
    debug = require('gulp-debug'),
	reload = browserSync.reload;

gulp.task('styles:doc', function() {
	return gulp.src(['src/assets/css/main.pcss', 'src/components/**/*.css', 'src/components/**/*.pcss', 'src/assets/css/frame-custom.pcss'])
		.pipe(plumberNotifier())
		.pipe($.sourcemaps.init())
		.pipe($.postcss([
			atImport,
			inlineBase64(),
			cssnext,
			colorguard({
				allowEquivalentNotation: true,
				threshold: 1
			}),
			inlineSvg({
				path: '/src/assets/css/'
			}),
			cachebuster({
				cssPath: '/src/assets/css/'
			}),
			reporter
		]))
		.pipe($.concat('style.css'))
		.pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest('docs/iframes/assets/css'));
});

gulp.task('styles:prod', function() {
	return gulp.src(['src/assets/css/main.pcss', 'src/components/**/*.css', 'src/components/**/*.pcss', 'src/assets/css/frame-custom.pcss'])
		.pipe(plumberNotifier())
		.pipe($.postcss([
			atImport,
			inlineBase64(),
			cssnext,
			colorguard({
				allowEquivalentNotation: true,
				threshold: 1
			}),
			inlineSvg({
				path: '/src/assets/css/'
			}),
			cachebuster({
				cssPath: '/src/assets/css/'
			}),
			cssnano({
				autoprefixer: false,
				discardUnused: {
					fontFace: false
				}
			})
		]))
		.pipe($.concat('style.min.css'))
		.pipe(gulp.dest('docs/dist/assets/css'))
		.pipe($.size({
			title: 'Stylesheet'
		}));
});

gulp.task('js:doc', function() {
	let b = browserify('src/assets/js/main.js', { debug: true })
		.add(require.resolve('babel-polyfill'))
		.transform('babelify',{
			plugins: ['transform-object-assign'],
			presets: ['latest']
	});
	return b.bundle()
		.pipe(source('main.js'))
		.pipe(buffer())
		.pipe($.sourcemaps.init({
			loadMaps: true
		}))
		.pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest('docs/iframes/assets/js'));
});

gulp.task('js:prod', function() {
	return browserify('src/assets/js/main.js', { debug: false })
		.add(require.resolve('babel-polyfill'))
		.transform('babelify',{
			plugins: ['transform-object-assign'],
			presets: ['latest'],
			sourceMaps: false
		})
		.bundle()
		.pipe(source('main.min.js'))
		.pipe(buffer())
		.pipe($.uglify())
		.pipe(gulp.dest('docs/dist/assets/js'))
		.pipe($.size({
			title: 'JavaScript'
		}));
});

gulp.task('copy:images:doc', function() {
	return gulp.src(['src/assets/img/**'])
		.pipe($.imagemin({
	      progressive: true,
	      interlaced: true,
	      svgoPlugin: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]
	    }))
		.pipe(gulp.dest('docs/iframes/assets/img'));
});

gulp.task('copy:images:prod', function() {
	return gulp.src(['src/assets/img/**'])
	    .pipe($.imagemin({
	      progressive: true,
	      interlaced: true
	    }))
		.pipe(gulp.dest('docs/dist/assets/img'))
		.pipe($.size({
			title: 'Images'
		}));
});

gulp.task('copy:fonts:doc', function() {
	return gulp.src(['src/assets/fonts/**'])
		.pipe(gulp.dest('docs/iframes/assets/fonts'));
});

gulp.task('copy:fonts:prod', function() {
	return gulp.src(['src/assets/fonts/**'])
			.pipe(gulp.dest('docs/dist/assets/fonts'))
			.pipe($.size({
				title: 'fonts'
			}));
});

gulp.task('copy:video', function() {
	return gulp.src(['src/assets/video/**'])
		.pipe(gulp.dest('docs/iframes/assets/video'));
});

gulp.task('copy:mock', function() {
    return gulp.src(['src/json-mock/**/*'])
        .pipe(gulp.dest('docs/json-mock'));
});

gulp.task('docs:generate', function() {
	return $.run('npm run topdoc').exec();
});

gulp.task('clean:docs', function() {
	return gulp.src(['docs'], {read: false})
		.pipe($.clean());
});

gulp.task('clean:dist', function() {
	return gulp.src(['docs/dist'], {read: false})
		.pipe($.clean());
});

// gulp.task('archive', function () {
// 	return gulp.src('docs/dist/**')
// 		.pipe($.archiver('archive.zip'))
// 		.pipe(gulp.dest('docs'))
// 		.pipe($.size({
// 			title: 'Archive'
// 		}));
// });

// Watch files for changes & reload
gulp.task('serve', function() {
	browserSync({
		port: 5000,
		notify: false,
		logPrefix: 'Server',
		// Run as an https by uncommenting 'https: true'
		// Note: this uses an unsigned certificate which on first access
		//       will present a certificate warning in the browser.
		// https: true,
		server: {
			baseDir: ['docs']
		}
	});

	gulp.watch(['src/**/*.js'], ['js:doc', reload]);
	gulp.watch(['src/**/*.pcss'], ['styles:doc', reload]);
	gulp.watch(['src/assets/img/**/*'], ['copy:images:doc', reload]);
	gulp.watch(['src/assets/fonts/**/*'], ['copy:fonts:doc', reload]);
	gulp.watch(['src/**/*', '!src/assets/img/**/*', '!src/**/*.js', '!src/**/*.pcss'], ['docs:generate', 'styles:doc', reload]);
	gulp.watch(['src/json-mock/**/*'], ['copy:mock', reload]);
});

gulp.task('default', ['clean:docs'], function (done) {
	runSequence(
		['copy:images:doc'],
		['docs:generate', 'styles:doc', 'js:doc', 'copy:fonts:doc', 'copy:video', 'copy:mock'],
		['serve'],
		done
	);
});

gulp.task('build', ['clean:dist'], function (done) {
	runSequence(
		['copy:images:prod'],
		['docs:generate', 'styles:prod', 'js:prod', 'copy:fonts:prod', 'copy:video', 'copy:mock'],
		done
	);
});