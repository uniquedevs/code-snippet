# Installation

Clone the repo.

You will need to install Node.js, if you haven't already.

After you've set this stuff up please run

	$ npm install -g gulp gulp-cli bower

Afterwards please run

	$ npm install
	
and then

	$ bower install

in your project's directory.
This will install all the things you need for running the gulp-tasks
automatically.

### Troubleshooting

If running the install does not work, please try running it as with
admin-rights:

	$ sudo npm install -g gulp gulp-cli bower

# Project structure
    .
    ├─ bower_components             # - A directory where Bower installs packages
    ├─ docs                         # - Main directory, where documentation on components is built. 
    │                               # When deploying a project server should look at this folder. 
    ├─ node_modules                 # - A directory where Node.js installs packages. 
    ├─ src                          # A folder with source files. It is a work directory. 
    │   ├─ assets                   # 
	│   │   ├─ css                  # 
	│   │   │   ├─ frame-custom.pcss# - CSS file that links up only for components and not present on production
	│   │   │   ├─ main.pcss        # - Main CSS file. All other css files should be linked to it except for 
    │   │   │   │                   # components files. Components file are linked automatically after main.css
	│   │   │   └─ *.pcss            # - Any css files. In order to include them,
	│   │   │                       # they should be written in main.css by `@import` property.
	│   │   ├─ fonts                # - A folder with fonts. 
	│   │   ├─ img                  # - A folder with images.
	│   │   └─ js                   #
	│   │       ├─ main.js          # - Main JS file. Is an entry point for all JS files on the project.
	│   │       └── *.js            # - Any other JS files. They have to be included using ES6 'import' syntax in 
    │   │                           # main.js or in components files.
    │   └─ components               # - A folder with components. 
    │       └─ <comp-name>          # - A folder with componenets' names. It shouldn't include spaces.
    │           ├─ *.js             # - JS file for componenets. Not mandatory.
    │           │                   # If necessary it has to be included in main.js using ES6 'import' syntax.
    │           ├─ <comp-name>.css  # - YAML marking(see below) 
    │           │                   # .
    │           ├─ *.html           # - HTML code of a component.
    │           │                   #
    │           └─ *.pcss           # - CSS styles of a component. Using Postcss   
    ├─ templates                    # - Tools and utilities. Please do not update anyhting in this folder without 
    │                               # a special need.
    ├─ create-component.js          # - The utility for creation of a component.
    ├─ gulpfile.js
    ├─ .gitignore
    ├─ package.json 
    ├─ bower.json
    └─ README.md

## How to create a new component?
Go to project's directory.

Run `npm run create` command in the terminal. 

Terminal will ask for component `name` and `description`.

`Name` is a mandatory parameter. `Description` isn't a mandatory, but we recommend filling it with the information that could be usefull for other developers. 
This will create a folder with files for the new component in `src/components/<component-name>`.

### Development workflow
Branch from `develop`, into `component/component-name`. On this branch you can do your component development.

Create new component using `run npm create` command. 
Run the server with `gulp`command.

In command string you have to see process of assembly and launch of the server which will display a message similar to:
	
	[Server] Access URLs:
	------------------------------------
	      Local: http://localhost:5000
	   External: http://10.10.10.93:5000
	------------------------------------
	         UI: http://localhost:3001
	UI External: http://10.10.10.93:3001
	------------------------------------

The server will automatically open a browser with 'http://localhost:5000' link. On the url provided by server one can connect to the app from any device given that they are in the same network.
UI of the url is required for synchronization of devices and debugging..
Any change in the `src` folder will cause process of reassembly and will refresh the page on all devices as soon as assembly is finished. 

Component's CSS will be included automatically in the course of assembly. It is extremely desirable that there is only 1 css file in the folder with a component. 
If there will be more files, it's not likely for something to break, but you will see that another page of documentation was generated on the basis of other file, that is most likely at all not what you have expected. Besides it violates the arrangement on files structure.
If due to some reasons there is an urgent need to use additional files, you can create them in the 'src/assets/css' folder and include it in the file of  component's styles. For example: `@import "../../assets/css/vars.css";`. 

Contrariwise component's JS opposite is not  included automatically. If necessary it has to be included in the  `src/assets/js/main.js` file using ES6 `import`.
There can be a lot of files since on the basis of js files nothing is included or generated.
 
It is more convenient to develop component in a separate window. For this purpose on the website find a necessary component and click "Open a new tab", or follow the link `http://localhost:5000/iframes/<component-name>.html` 
If to you need to look at a component at different resolutions, you can use external link (example: `http://10.10.10.93:5000/iframes/<component-name>.html`) 
in order to open a component on other device or to use "different screen sizes" tool which is built in Google Chrome and Firefox. Another option is to change the iframe size for the necessary component on the documentation page. 

For each component described in CSS comments to the `body` tag we add `id` which is its slug name. E.g. if the component was called 'Disabled Select', then `<body>`tage will become `<body id="disabled-select">`. 
It is possible to use this `id` in `src/assets/css/frame-custom.css` in order to correct displaying of component in the documentation. E.g. to center component on the screen, but to exclude these changes from production version. 

After work on a component is finished it is necessary to merge code into `develop` branch and to collect assets (js, css, img, fonts) into archive so that other developers could download it.
For this purpose one should start `gulp build` command and wait for it to finish. Then `git push` and we are done.

Doesn't look complicated, right?

# Technologies 
### CSS
`IMPORTANT:` For generation of documentation and component comments from CSS declared in `YAML` are used ([more on this can be found in HTML section](#markdown-header-html)).
If comment are not present in CSS or are written with a mistake, the empty page will be generated,
since the parser will consider that there are no components.
Also if data with nested structure is needed next is important to know:

 > In YAML block styles, structure is determined by indentation. In general, indentation is defined as a zero or more space characters at the start of a line.
 To maintain portability, tab characters must not be used in indentation, since different systems treat tabs differently.

`postcss` with a set of plug-ins are used in the project. Here is their list and documentation:
 
 1) [CSSNext](http://cssnext.io/features/)
 
 2) [PostCSS Import](https://github.com/postcss/postcss-import)
 
 3) [PostCSS Cachebuster](https://github.com/glebmachine/postcss-cachebuster)
 
 4) [PostCSS Inline SVG](https://github.com/TrySound/postcss-inline-svg)
 
 5) [CSS Colorguard](https://github.com/SlexAxton/css-colorguard)
 
 6) [PostCSS Browser Reporter](https://github.com/postcss/postcss-browser-reporter)
 
 7) [PostCSS Inline Base64](https://github.com/lagden/postcss-inline-base64)
 
### HTML
It is possible to use `underscore templates` in HTML files, or to write a pure HTML code.
All parameters declared in CSS comments in `YAML` notation are transferred to the template.
This can be used to reduce amount of marking or to use the same html file for several components. For example:

Let's say we have a `select` component which contains the following code in css file:
	
	/* topdoc
    name: Enabled Select
    description: Enabled dropdown select
    markup: |
        @include select.html
    disabled: false
    */
    
    select {}
    
    /* topdoc
    name: Disabled Select
    description: Disabled dropdown select
    markup: |
        @include select.html
    disabled: true
    */
    
    select:disabled {}
    
And `select.html` file with next code:

	<select name="select"<%= disabled ? ' disabled' : '' %>>
	    <option value="" selected>Value 1</option>
	    <option value="value2">Value 2</option>
	</select>
	
This code will generate `select.html` page with the description and references to 2 components:

`enabled-select.html`

	<select name="select">
	    <option value="" selected>Value 1</option>
	    <option value="value2">Value 2</option>
	</select>

`disabled-select.html`

	<select name="select" disabled>
	    <option value="" selected>Value 1</option>
	    <option value="value2">Value 2</option>
	</select>

In the course of directive  assembly `@include select.html` will be replaced with the compiled HTML code from the specified file.
If the file is not found this directive will be included as a HTML code into 'enabled-select.html' and 'disabled-select.html' files.

In certain cases, for components with a small amount of marking it is possible to neglect HTML files, i.e. the component can consist of only 1 css file.

	/* topdoc
    name: Input Text Normal
    markup: <input type="text">
    */
    
    input[type="text"]{}
    
    /* topdoc
    name: Input Text Disabled
    markup: <input type="text" disabled>
    */
    
    input[type="text"]:disabled{}
    
2 html files with the corresponding marking will be generated based on this code

### JS
Pure Javascript including polyfill for ES6, ES7 features.

Bower is necessary to manage libraries. For example, if you need `jquery` you can install it using Bower and include it in some of the components `import $ from '../../../bower_components/jquery/dist/jquery.min';`.
In case it is required to update libraries, you will need to go to root of the project and to launch `bower update` from the terminal.


# Methodologies
You can use any methodology that you prefer. 
But remember that components have to be composable, therefore you have to avoid inheritance of styles as much as possible.

#####Be intentional
The best way to make sure your selectors don’t style unwanted elements is to not give them the opportunity. 

	/* Grenade */
	#main-nav ul li ul { }
	
	/* Sniper Rifle */
	.subnav { }
	
#####Namespace your classes
Namespacing your classes keeps your components self-contained and modular.

	/* High risk of style cross-contamination */
    .widget { }
    .widget .title { }
    
    /* Low risk of style cross-contamination */
    .widget { }
    .widget-title { }
    
#####Extend components with modifier classes
When an existing component needs to look slightly different in a certain context, create a modifier class to extend it.

	/* Bad */
    .widget { }
    #sidebar .widget { }
    
    /* Good */
    .widget { }
    .widget-sidebar { }