import $ from '../../../bower_components/jquery/dist/jquery.min';

console.log("dsfsdfdsf");

function toggleAccordion(event) {
	if (event) {
		event.preventDefault();
	}

	let panel = this.nextElementSibling;
	if (panel.style.maxHeight) {
		this.classList.remove("active");
		panel.style.maxHeight = null;
	} else {
		this.classList.add("active");
		panel.style.maxHeight = panel.scrollHeight + "px";
	}
}

module.exports = function(){
	let $accordion = $('.js-simple-accordion-heading');

	if ($accordion.length)
		$accordion.on('click', toggleAccordion);
};