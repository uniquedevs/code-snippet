import AutoComplete from './autocomplete';

module.exports = function(){
	let categories = ['Level 1', 'Level 2', 'California', 'Ukraine', 'Estonia'];

	let auto_complete = new AutoComplete({
		selector: 'input[name="search"]',
		minChars: 3,
		source: function(term, suggest){
			term = term.toLowerCase();
			let matches = [];
			for (let i=0; i<categories.length; i++)
				if (~categories[i].toLowerCase().indexOf(term)) matches.push(categories[i]);
			suggest(matches);
			return matches.length;
		}
	});
};