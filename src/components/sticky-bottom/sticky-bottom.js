import $ from '../../../bower_components/jquery/dist/jquery.min';

module.exports = function(){

	if (!$('.booking-summary--info') || $('.booking-summary--info').length === 0) return;

	const
			$sticky_element = $('.booking-summary--info'),
			sticky_offset_top = $sticky_element.offset().top,
			sticky_height = $sticky_element.height(),
			window_height = $(window).height()
	;

	function positionSticky(){
		let sticky_offset = sticky_offset_top - $(window).scrollTop(),
			sticky_above_bottom =  window_height - sticky_height - sticky_offset < 0
		;

		$sticky_element.toggleClass('sticky', sticky_above_bottom);
	}

	$(window).on('scroll', positionSticky);

	positionSticky();
};