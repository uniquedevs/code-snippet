import $ from '../../../bower_components/jquery/dist/jquery.min';

import SimpleAccordion from '../../components/simple-accordion/simple-accordion.js';
import StickyBottom from '../../components/sticky-bottom/sticky-bottom.js';
import Typeahead from '../../components/typeahead/typeahead.js';

window.jQuery = $;

$(function(){
	SimpleAccordion();
	StickyBottom();
	Typeahead();
});
