const prompt = require('prompt');
const fs = require('fs');
const path = require('path');
const fse = require('fs-extra');

function renameFilesRecursive(dir, from, to) {
	fs.readdirSync(dir).forEach(it => {
		const itsPath = path.resolve(dir, it),
		      itsStat = fs.statSync(itsPath);

		if (itsStat.isDirectory()) {
			renameFilesRecursive(itsPath)
		} else {

			if (itsPath.search(from) != -1) {
				fs.renameSync(itsPath, itsPath.replace(from, to))
			}
		}
	})
}

prompt.start();

prompt.get(['name', 'description'], function (err, result) {
	const source = 'templates/component',
	      dist = 'src/components';

	if(err){
		return err;
	}

	if(!result.name){
		return new Error('Name is required!');
	}

	try {
		let component_safe_name = result.name.toLowerCase().split(' ').join('-'),
			component_path = `${dist}/${component_safe_name}`,
			component_dir = path.resolve(__dirname, component_path);

		fse.mkdirs(component_dir);
		fse.copySync(source, component_path);
		renameFilesRecursive(component_dir, /example/, component_safe_name);

		let css_file_path = path.resolve(__dirname, `${component_path}/${component_safe_name}.css`);

		if(fs.existsSync(css_file_path)){
			let text = fs.readFileSync(css_file_path, 'utf8')
						.replace('@name', result.name)
						.replace('@description', result.description || '')
						.replace('@safe_name', component_safe_name);

			fs.writeFileSync(css_file_path, text, 'utf8');
		}

		console.log("success!")
	} catch (err) {
		console.error(err)
	}
});