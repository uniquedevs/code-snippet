window.onload = function(){
	let showCodeDivs = Array.from(document.getElementsByClassName('showcode')),
	    frames = Array.from(document.getElementsByClassName('componentframe')),
	    copyLinks = Array.from(document.getElementsByClassName('copy-to-clipboard')),
	    slideMenuButton = document.getElementById('slide-menu-button'),
	    docNav = document.getElementById('docNav'),
	    pageNav = document.getElementById('pageNav'),
	    pageLinks = Array.from(pageNav.getElementsByTagName('a')),
	    site = document.getElementById('site');

	showCodeDivs.forEach(linkHolder => {
		linkHolder.addEventListener('click', toggleCodeBlock.bind(linkHolder));
	});

	frames.forEach(frame => {
		let iframeContext = frame.firstChild.contentWindow;
		iframeContext.addEventListener('resize', onResize.bind(frame));
		iframeContext.dispatchEvent(new Event('resize'));
	});

	slideMenuButton.onclick = function(event) {
		let cl = site.classList;

		if (cl.contains('open')) {
			cl.remove('open');
		} else {
			cl.add('open');
		}
	};

	pageLinks.forEach(function(link) {
		link.onclick = function(event){
			let cl = site.classList;

			if (cl.contains('open')) {
				cl.remove('open');
			}
		};
	});

	copyLinks.forEach(link => link.onclick = copyCode);

	docNav.onchange = route;
};

function copyCode(event){
	event.preventDefault();

	let text = event.target.parentNode.nextSibling.innerText,
	    textArea = document.createElement("textarea");

	textArea.style.position = 'fixed';
	textArea.style.top = 0;
	textArea.style.left = 0;
	textArea.style.width = '2em';
	textArea.style.height = '2em';
	textArea.style.padding = 0;
	textArea.style.border = 'none';
	textArea.style.outline = 'none';
	textArea.style.boxShadow = 'none';
	textArea.style.background = 'transparent';


	textArea.value = text;

	document.body.appendChild(textArea);

	textArea.select();

	try {
		let successful = document.execCommand('copy');

		event.target.innerText = 'Copied';

		setTimeout(function(){
			event.target.innerText = 'Copy';
		}, 1000 * 5);
	} catch (err) {
		alert('Oops, unable to copy');
	}

	document.body.removeChild(textArea);
}

function route(event){
	window.location.href = event.target[event.target.selectedIndex].value;
}

function onResize(event){
	let infoBlock = this.previousSibling,
	    target = event.target;

	infoBlock.firstChild.innerHTML = target.innerWidth;
	infoBlock.lastChild.innerHTML = target.innerHeight;
}

function toggleCodeBlock(event){
	event.preventDefault();

	let is_link = event.target.tagName.toLowerCase() === 'a';

	if(!is_link){
		return false;
	}

	let element = this.nextSibling,
	    style = window.getComputedStyle(element);

	if(style.getPropertyValue('display') == 'none'){
		event.target.innerHTML = 'Hide snippet code';
		element.style.display = 'block';
	} else {
		event.target.innerHTML = 'Show snippet code';
		element.style.display = 'none';
	}

	return false;
}
