'use strict';

const _ = require('underscore');

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = defaultTemplate;

const _pug = require('pug'),
	  _pug2 = _interopRequireDefault(_pug),
	  _path = require('path'),
	  _path2 = _interopRequireDefault(_path),
	  _fsExtra = require('fs-extra'),
	  _fsExtra2 = _interopRequireDefault(_fsExtra),
	  fs = require('fs'),
	  path = require('path');

_.mixin({
	inject: function(path){
		return fs.readFileSync(path, 'utf8', (err, data) => data || '');
	}
});

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

/**
 *  Private: replaces the extension of a file path string with a new one.
 *
 *  * `npath` {String} path to file.
 *  * `ext` {String} new extension to replace the old one.
 *
 *  Returns {String} with replaced extension
 */
function _replaceExt(npath, ext) {
	if (typeof npath !== 'string' || npath.length === 0) {
		return npath;
	}

	let nFileName = _path2.default.basename(npath, _path2.default.extname(npath)) + ext;

	return _path2.default.join(_path2.default.dirname(npath), nFileName);
}

/**
 *  Public: creates docs using topDocument data with a pug template.
 *
 *  * `topDocument` {TopDocument} result from topdoc parsing.
 *
 */

/* eslint-disable no-console */
function defaultTemplate(topDocument) {

	try {
		let content, newFileName, tplFileIndex, tplFileIframe;

		tplFileIndex = _path2.default.resolve(__dirname, 'index.jade');
		tplFileIframe = _path2.default.resolve(__dirname, 'iframe.jade');

		// Generate single component files
		topDocument.components.forEach(component => {
			let frame_path = topDocument.destination + 'iframes',
			    componentName = component.name.toLowerCase().split(' ').join('-') + '.html';

			_fsExtra2.default.mkdirsSync(_path2.default.resolve(frame_path));

			component.iframe_path = 'iframes/' + componentName;

			// Include markup from html file to component
			if(component.markup){
                component.markup = component.markup.replace(/@include.+\.html/gi, (directive) => {
                    let file_name = directive.split(' ').pop(),
						file_path = _path2.default.resolve(path.parse(topDocument.sourcePath).dir + '/' + file_name);

                    if(fs.existsSync(file_path)){
                        let markup = fs.readFileSync(file_path, 'utf8'),
                            tpl = _.template(markup);

                        return tpl(component);
                    }
                });
			}

			content = _pug2.default.renderFile(tplFileIframe, {
				document: topDocument,
				component: component
			});

			// Create HTML files with content
			_fsExtra2.default.writeFileSync(_path2.default.resolve(frame_path, componentName), content);
		});

		// Replace CSS ext to HTML
		topDocument.files.forEach(function (file) {
			file.filename = _replaceExt(file.filename, '.html');
		});

		// Generate file content
		content = _pug2.default.renderFile(tplFileIndex, { document: topDocument });

		// Create dir structure
		_fsExtra2.default.mkdirsSync(_path2.default.resolve(topDocument.destination));

		// Create new file name
		newFileName = topDocument.first ? 'index.html' : _replaceExt(topDocument.filename, '.html');

		// Create HTML files with content
		_fsExtra2.default.writeFileSync(_path2.default.resolve(topDocument.destination, newFileName), content);
	} catch (err) {
		console.log(err);
	}
}

module.exports = exports['default'];